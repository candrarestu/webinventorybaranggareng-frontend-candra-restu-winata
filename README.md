Web Sistem Informasi Inventaris Perusahaan Gareng


List Menu:
1. Dashboard
2. Barang Masuk
3. Barang Keluar
4. Peminjaman

Penjelasan menu-menu Web:
1. Dashboard --> Berisi jumlah total data barang, total barang yang masuk, total barang yang keluar, dan data peminjaman inventaris.
2. Barang Masuk --> Menu yang dapat digunakan untuk mengelola semua barang inventaris yang masuk.
3. Barang Keluar --> Menu yang dapat digunakan untuk mengelola semua barang inventaris yang keluar.
4. Peminjaman --> Menu yang dapat digunakan untuk mengelola semua barang yang akan dipinjam atau telah dikembalikan.

